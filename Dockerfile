FROM nginx:alpine
EXPOSE 8080
COPY /travel_agency-html/* /travel/
COPY travel.conf /etc/nginx/conf.d/default.conf
VOLUME /travel

